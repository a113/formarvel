//
//  ListTableViewCell.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

protocol LikeDislikeClickedDelegate {
    func dislikeLikeClickedDelegate(tag: Int, isLiked: Bool)
}

class ListTableViewCell: UITableViewCell {
    var delegateLikeDislikeClickedDelegate: LikeDislikeClickedDelegate?
    @IBOutlet weak var curImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var charactersLabel: UILabel!
    @IBOutlet weak var creatorsLabel: UILabel!
    var swipedCellIndex: IndexPath = IndexPath()
    var liked: Bool = false
    var currCharacters = Array<CharacterList>()
    var currCreators = Array<CreatorsList>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setWith(_ cmc: Comic) {
        
        if let n = cmc.curTitle as? String,
            n.count > 0/*,
            let str = String(describing: n.cString(using: String.Encoding.utf8)) as? String */ {
                nameLabel.text = String(describing: n)
        }
        self.button.tag = self.swipedCellIndex.row
        if cmc.favorite == false {
            self.button.setImage(UIImage(named:"notLiked"), for: UIControl.State.normal)
        } else {
            self.button.setImage(UIImage(named:"liked"), for: UIControl.State.normal)
        }
        let w1 = Array(Database.getCharactersWithComicId(id: cmc.id))
        //let charactersId = (Array(Database.getCharactersWithComicId(id: cmc.id)))[0].charactersId
        //currCharacters
        
        var chL: Array<String> = Array<String>()
        if w1.count > 0 {
            for curCh in w1[0].charactersId {
                chL.append("\(curCh.id)")
            }
            let w2 = Array(Database.getCharactersWithIdsArray(idsArray: chL, comicId: cmc.id))
            var ch: String = ""
            var ind = 0
            for curCh in w2 {
                if ind + 1 == w2.count {
                    ch.append("\(curCh.name)")
                } else {
                    if ind < w2.count {
                        ch.append("\(curCh.name), ")
                    }
                }
                ind = ind + 1
            }
            print("ch = \(ch)")
            charactersLabel.text = ch
        } else {
            charactersLabel.text = ""
        }
        
        var crL: Array<String> = Array<String>()
        let w3 = Array(Database.getCreatorsWithComicId(id: cmc.id))
        if w3.count > 0 {
            for curCr in w3[0].creatorsId {
                crL.append("\(curCr.id)")
            }
            let w4 = Array(Database.getCreatorsWithIdsArray(idsArray: crL, comicId: cmc.id))
            var cr: String = ""
            var ind = 0
            for curCr in w4 {
                if ind + 1 == w4.count {
                    cr.append("\(curCr.name)(\(curCr.role))")
                } else {
                    if ind < w4.count {
                        cr.append("\(curCr.name)(\(curCr.role)), ")
                    }
                }
                ind = ind + 1
            }
            print("cr = \(cr)")
            creatorsLabel.text = cr
        } else {
            creatorsLabel.text = ""
        }
        
        nameLabel.sizeToFit()
        //nameLabel.setNeedsDisplay()
        charactersLabel.sizeToFit()
        //charactersLabel.setNeedsDisplay()
        creatorsLabel.sizeToFit()
        //creatorsLabel.setNeedsDisplay()
        self.layoutIfNeeded()
        self.setNeedsDisplay()
    }
    
    @IBAction func likeDislikeClicked(_ sender: Any) {
        if self.delegateLikeDislikeClickedDelegate != nil {
            print("button tag = \(self.button.tag)")
            delegateLikeDislikeClickedDelegate?.dislikeLikeClickedDelegate(tag: self.button.tag, isLiked: !(self.liked))
            
        }
        if liked {
            self.button.setImage(UIImage(named:"notLiked"), for: UIControl.State.normal)
            
        } else {
            self.button.setImage(UIImage(named:"liked"), for: UIControl.State.normal)
        }
        liked = !liked
    }
    
    func clearCellData()  {
        self.nameLabel.text = nil
        self.nameLabel.isHidden = false
        self.charactersLabel.text = nil
        self.charactersLabel.isHidden = false
        self.creatorsLabel.text = nil
        self.creatorsLabel.isHidden = false
        self.curImageView.image = nil
    }
    
}
