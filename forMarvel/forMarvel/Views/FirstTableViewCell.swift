//
//  FirstTableViewCell.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/15/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

class FirstTableViewCell: UITableViewCell {
    
    var delegateLikeDislikeClickedDelegate: LikeDislikeClickedDelegate?
    @IBOutlet weak var curImageView: UIImageView!
    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var charactersLabel: UILabel!
    @IBOutlet weak var creatorsLabel: UILabel!
    var swipedCellIndex: IndexPath = IndexPath()
    var liked: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setWith(_ cmc: Comic) {
        if let n = cmc.curTitle as? String,
            n.count > 0 {
                nameLabel.text = String(describing: n)
        }
        self.favoriteImageView.tag = self.swipedCellIndex.row
        if cmc.favorite == false {
           favoriteImageView.image = UIImage(named:"notLiked")
        } else {
            favoriteImageView.image = UIImage(named:"liked")           
        }
        self.layoutIfNeeded()
    }
    
}
