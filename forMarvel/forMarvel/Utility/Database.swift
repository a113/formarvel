//
//  Database.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import RealmSwift

class Database {
    
    static let realm = try! Realm()
    var notificationToken: NotificationToken? = nil
    
    /// Set new realm for logged in user
    class func setDefaultRealmForUser(username: String)
    {
        var config = Realm.Configuration()
        
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(username).realm")
        
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
    class func saveEntities(entities: [Object])
    {
        try! realm.write {
            realm.add(entities, update: true)
        }
    }
    
    
    class func saveEntities(entities: [Object], updated: Bool)
    {
        try! realm.safeWrite {
            realm.add(entities, update: updated)
        }
    }
    
    class func delete(entity: Object)
    {
        try! realm.safeWrite {
            realm.delete(entity)
        }
    }
    
    class func getFavoritsAll() -> Results<Favorites> {
        return try! Realm().objects(Favorites.self).sorted(byKeyPath: "id", ascending: true)
    }
    
    class func getComicsWithId(id: String) -> Results<Comic> {
        let predicate = NSPredicate(format: "id == \"\(id)\"")
        let filtered = realm.objects(Comic.self).filter(predicate).sorted(byKeyPath: "curTitle", ascending: true)
        return filtered
    }
    
    class func getFavoritsWithId(id: String) -> Results<Favorites> {
        let predicate = NSPredicate(format: "id == \"\(id)\"")
        let filtered = realm.objects(Favorites.self).filter(predicate).sorted(byKeyPath: "id", ascending: true)
        return filtered
    }
    
    class func getCreatorsWithComicId(id: String) -> Results<CreatorsList> {
        let predicate = NSPredicate(format: "comicId == \"\(id)\"")
        let filtered = realm.objects(CreatorsList.self).filter(predicate)
        return filtered
    }
    
    class func getCharactersWithComicId(id: String) -> Results<CharacterList> {
        let predicate = NSPredicate(format: "comicId == \"\(id)\"")
        let filtered = realm.objects(CharacterList.self).filter(predicate)
        return filtered
    }
    
    class func getCharactersWithIdsArray(idsArray: Array<String>, comicId: String) -> Results<Character>
    {
        var predicateS: String = ""
        var ind = 0
        for curId in idsArray {
            if ind == 0/*,
                predicateS.count == 0*/
            {
                predicateS = "id == \"\(curId)\""
            } else {
                predicateS = predicateS + " OR id == \"\(curId)\""
            }
            ind = ind + 1
        }
        //http://gateway.marvel.com/v1/public/characters/1009156
        let filtered = realm.objects(Character.self).filter(predicateS).sorted(byKeyPath: "name", ascending: false)
        return filtered
    }
    
    class func getCreatorsWithIdsArray(idsArray: Array<String>, comicId: String) -> Results<Creator>
    {
        var predicateS: String = ""
        var ind = 0
        for curId in idsArray {
            if ind == 0/*,
                 predicateS.count == 0*/
            {
                predicateS = "id == \"\(curId)\""
            } else {
                predicateS = predicateS + " OR id == \"\(curId)\""
            }
            ind = ind + 1
        }
        let filtered = realm.objects(Creator.self).filter(predicateS).sorted(byKeyPath: "name", ascending: false)
        return filtered
    }

}
