//
//  ApiManager.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import UIKit
import Siesta
import SwiftyJSON


// Depending on your taste, a Service can be a global var, a static var singleton, or a piece of more carefully
// controlled shared state passed between pieces of the app.

let ApiManager = _ApiManager()

class _ApiManager: Service {
    
    // MARK: - Configuration
    //https://gateway.marvel.com:443/v1/public/comics?titleStartsWith=love&apikey=790968ec6983ad5051f3cb2edb50e50c
    //var getComics: Resource { return resource("/v1/public/comics?titleStartsWith=" + search + "/" + chosenCountryId)}
    var getComics: Resource { return resource("/v1/public/comics").withParam("titleStartsWith", search).withParam("apikey", apikey).withParam("hash", hashCur).withParam("ts", ts).withParam("limit", limitComics)}
    var search: String = ""
    var ts: String = ""
    var hashCur: String = ""
    var apikey: String = "790968ec6983ad5051f3cb2edb50e50c"
    var limitComics: String = ""
    
    /*
     var addReviewByBussinessCardId: Resource { return resource("/api/reviews/"+bussinessCard_id).withParam("token", token)}//post
     var addReviewByBussinessCardId: Resource { return resource("/api/reviews/"+bussinessCard_id).withParam("token", token)}//post
     var addReviewByBussinessCardId: Resource { return resource("/api/reviews/"+bussinessCard_id).withParam("token", token)}//post*/
    
    /*
     private let service = Service(
     baseURL: "http://174.129.92.49",
     standardTransformers: [.text, .image])  // No .json because we use Swift 4 JSONDecoder instead of older JSONSerialization
     */
    fileprivate init() {
        #if DEBUG
        // Bare-bones logging of which network calls Siesta makes:
        SiestaLog.Category.enabled = //[.network]
            SiestaLog.Category.all
        /* [.network, .networkDetails, .configuration, .pipeline, .staleness, .stateChanges, .observers, .cache]*/
        // For more info about how Siesta decides whether to make a network call,
        // and which state updates it broadcasts to the app:
        
        //LogCategory.enabled = LogCategory.common
        
        // For the gory details of what Siesta’s up to:
        
        //LogCategory.enabled = LogCategory.all
        
        // To dump all requests and responses:
        // (Warning: may cause Xcode console overheating)
        
        //LogCategory.enabled = LogCategory.all
        #endif
        
        // –––––– Global configuration ––––––
        
        _ = JSONDecoder()
        
        let url = "https://gateway.marvel.com:443"
        
        
        super.init(baseURL: url)
        
        configure {
            // Custom transformers can change any response into any other — including errors.
            // Here we replace the default error message with the one provided by the  API (if present).
            
            
            $0.pipeline[.parsing].add(SwiftyJSONTransformer, contentTypes: ["*/json"])
            
            $0.pipeline[.cleanup].add(
                ApiErrorMessageExtractor())
            
            
        }
        
        configureTransformer("/v1/public/comics?titleStartsWith=") {
            ComicsResponse(json: $0.content)
        }
        
        
        // –––––– Auth configuration ––––––
        
        // Note the "**" pattern, which makes this config apply only to subpaths of baseURL.
        // This prevents accidental credential leakage to untrusted servers.
        
        configure("**") {
            // This header configuration gets reapplied whenever the user logs in or out.
            // How? See the basicAuthHeader property’s didSet.
            
            $0.headers["Authorization"] = self.basicAuthHeader
        }
        
    }
    
    // MARK: - Authentication
    
    func logIn(username: String, password: String) {
        if let auth = "\(username):\(password)".data(using: String.Encoding.utf8) {
            basicAuthHeader = "Basic \(auth.base64EncodedString())"
        }
    }
    
    func logOut() {
        basicAuthHeader = nil
    }
    
    var isAuthenticated: Bool {
        return basicAuthHeader != nil
    }
    
    private var basicAuthHeader: String? {
        didSet {
            // These two calls are almost always necessary when you have changing auth for your API:
            
            invalidateConfiguration()  // So that future requests for existing resources pick up config change
            wipeResources()            // Scrub all unauthenticated data
            
            // Note that wipeResources() broadcasts a “no data” event to all observers of all resources.
            // Therefore, if your UI diligently observes all the resources it displays, this call prevents sensitive
            // data from lingering in the UI after logout.
        }
    }
    
    // MARK: - Endpoint Accessors
    
    // You can turn your REST API into a nice Swift API using lightweight wrappers that return Siesta resources.
    //
    // Note that this class keeps its Service private, making these methods the only entry points for the API.
    // You could also choose to subclass Service, which makes methods like service.resource(…) available to
    // your whole app. That approach is sometimes better for quick and dirty prototyping.
    //
    // If this section gets too long for your taste, you can move it to a separate file by putting a helper method
    // in an extension.
    /*
     func createAuthToken() -> Request {
     return tokenCreationResource
     .request(.post, json: userAuthData())
     .onSuccess {
     self.authToken = $0.jsonDict["token"] as? String  // Store the new token, then…
     self.invalidateConfiguration()                    // …make future requests use it
     }
     }
     */
    
    
}

// MARK: - Custom transformers

/// If the response is JSON and has a "message" value, use it as the user-visible error message.
private struct ApiErrorMessageExtractor: ResponseTransformer {
    
    func process(_ response: Response) -> Response
    {
        switch response
        {
        case .success:
            return response
            
        case .failure(var error):
            // Note: the .json property here is defined in Siesta+SwiftyJSON.swift
            error.userMessage = error.json["message"].string ?? error.userMessage
            print("%@", error.userMessage)
            return .failure(error)
        }
    }
    
    private struct ApiErrorEnvelope: Decodable {
        let message: String
    }
}

/// Special handling for detecting whether repo is starred; see "/user/starred/*/*" config above
private struct TrueIfResourceFoundTransformer: ResponseTransformer {
    func process(_ response: Response) -> Response {
        switch response {
        case .success(var entity):
            entity.content = true         // Any success → true
            return logTransformation(
                .success(entity))
            
        case .failure(let error):
            if error.httpStatusCode == 404, var entity = error.entity {
                entity.content = false    // 404 → false
                return logTransformation(
                    .success(entity))
            } else {
                return response           // Any other error remains unchanged
            }
        }
    }
}

let SwiftyJSONTransformer =
    ResponseContentTransformer (transformErrors: true)
    {
        JSON($0.content as AnyObject)
        
}

extension TypedContentAccessors
{
    var json :JSON {
        return typedContent(ifNone: JSON.null)
        
    }
}

