//
//  UIImage+Extension.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/15/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//
import UIKit

extension UIImage {
    func getImageRatio() -> CGFloat {
        let imageRatio = CGFloat(self.size.width / self.size.height)
        return imageRatio
    }
}
