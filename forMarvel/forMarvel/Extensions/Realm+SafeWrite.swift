//
//  Realm+SafeWrite.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit
import RealmSwift

extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
    
    public func writeAsync<T : ThreadConfined>(obj: T, errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }, block: @escaping ((Realm, T?) -> Void)) {
        let wrappedObj = ThreadSafeReference(to: obj)
        let config = Realm.Configuration.defaultConfiguration
        DispatchQueue(label: "background").async {
            autoreleasepool {
                do {
                    let realm = try Realm(configuration: config)
                    let obj = realm.resolve(wrappedObj)
                    
                    try realm.write {
                        block(realm, obj)
                    }
                }
                catch {
                    errorHandler(error)
                }
            }
        }
    }
    
    
    
}

extension DispatchQueue {
    public static let realmBackgroundQueue = DispatchQueue(label: "io.realm.realm.background")
}
extension ThreadSafeReference {
    func async(write: Bool = false, queue: DispatchQueue = .realmBackgroundQueue,
               errorHandler: ((Realm.Error) -> Void)? = nil,
               block: @escaping (Realm, Confined) -> Void) {
        queue.async {
            do {
                let realm = try Realm()
                if let obj = realm.resolve(self) {
                    if write { realm.beginWrite() }
                    block(realm, obj)
                    if write { try realm.commitWrite() }
                } else {
                    // throw "object deleted" error
                }
            } catch {
                errorHandler?(error as! Realm.Error)
            }
        }
    }
}

