//
//  DetailsViewController+UITableViewDelegate.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/15/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

extension DetailsViewController : UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("heightForRowAt")
            /*if curImage != nil {
                let imageRatio = curImage.getImageRatio()
                print("height = \(tableView.frame.width / imageRatio)")
                //let cell = curTableView.cellForRow(at: indexPath) as? ListTableViewCell
                return (tableView.frame.width / imageRatio)// + CGFloat(h)
            } else {*/
                return UITableView.automaticDimension
            //}
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("numberOfSections")
        return 1
    }
    
    // UITableViewDataSource protocol methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection")
            return 1        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Dequeue cell
        print("det cellForRowAt")
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell", for: indexPath) as! ListTableViewCell
            cell.delegateLikeDislikeClickedDelegate = self
        cell.clearCellData()
            cell.swipedCellIndex = indexPath
        cell.curImageView.image = curImage
        cell.setWith(curCmc)
        cell.nameLabel.sizeToFit()
        cell.nameLabel.setNeedsDisplay()
        cell.charactersLabel.sizeToFit()
        cell.charactersLabel.setNeedsDisplay()
        cell.creatorsLabel.sizeToFit()
        cell.creatorsLabel.setNeedsDisplay()
        
        if let c = cell.nameLabel?.frame.size.height {
            h = h + Double(c)
        } else {
            if let c = cell.nameLabel?.frame.height {
                h = h + Double(c)
            }
        }
        if let c = cell.charactersLabel?.frame.size.height {
            h = h + Double(c)
        } else {
            if let c = cell.charactersLabel?.frame.height {
                h = h + Double(c)
            }
        }
        if let c = cell.creatorsLabel?.frame.size.height {
            h = h + Double(c)
        } else {
            if let c = cell.creatorsLabel?.frame.height {
                h = h + Double(c)
            }
        }
        h = h + 16.0 + 30.0
        print("h = \(h)")
            return cell
    }

}
