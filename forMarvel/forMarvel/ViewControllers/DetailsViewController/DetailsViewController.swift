//
//  DetailsViewController.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, LikeDislikeClickedDelegate {
    
    var selectedComicId: String = ""
    var curCmc: Comic = Comic()
    var curImage : UIImage = UIImage()
    @IBOutlet weak var curTableView: UITableView!
    var h = 0.0
    var savedNumber: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        curTableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "listTableCell")

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        savedNumber = 0
        curCmc = Database.getComicsWithId(id: String(selectedComicId))[0]
        if let p = curCmc.imageLink?.path,
            let ex = curCmc.imageLink?.curExtension,
            let all = "\(p).\(ex)" as? String {
            let arStringSplited = all.components(separatedBy: "i/mg/")
            if arStringSplited.count > 1 {
                let searchS = arStringSplited[1]
                print("searchS = \(searchS)")
                if let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                    let image = self.getSavedImage(named:s) {
                    curImage = image
                    curTableView.reloadData()
                } else {
                    if let p = curCmc.thumbnail?.path,
                        let ex = curCmc.thumbnail?.curExtension,
                        let all = "\(p).\(ex)" as? String {
                        let arStringSplited = all.components(separatedBy: "i/mg/")
                        if arStringSplited.count > 1 {
                            let searchS = arStringSplited[1]
                            print("searchS = \(searchS)")
                            if let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                                let image = self.getSavedImage(named:s) {
                                curImage = image
                                curTableView.reloadData()
                            }
                        }
                    }
                }
            }
        } else {
            if let p = curCmc.thumbnail?.path,
                let ex = curCmc.thumbnail?.curExtension,
                let all = "\(p).\(ex)" as? String {
                let arStringSplited = all.components(separatedBy: "i/mg/")
                if arStringSplited.count > 1 {
                    let searchS = arStringSplited[1]
                    print("searchS = \(searchS)")
                    if let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                        let image = self.getSavedImage(named:s) {
                        curImage = image
                        curTableView.reloadData()
                    }
                }
            }
        }
        
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    func dislikeLikeClickedDelegate(tag: Int, isLiked: Bool){
        if let curC = curCmc as? Comic {
            Database.realm.beginWrite()
            curC.favorite = isLiked
            try! Database.realm.commitWrite()
            
            if isLiked == true {
                //Database.realm.beginWrite()
                let fv = Favorites(idCur: curC.id)
                Database.saveEntities(entities: [fv])
                //try! Database.realm.commitWrite()
            }
            if isLiked == false {
                let fv: Favorites = Database.getFavoritsWithId(id: String(curC.id))[0]
                Database.delete(entity: fv)
            }
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if savedNumber < 5 {
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.curTableView.reloadData()
                self.savedNumber = self.savedNumber + 1
            })
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
