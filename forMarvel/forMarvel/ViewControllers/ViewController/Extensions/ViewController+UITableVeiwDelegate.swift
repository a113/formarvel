//
//  ViewController+UITableVeiwDelegate.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return UITableView.automaticDimension
        print("height")
        if images.count > indexPath.row {
            let currentImage = images[indexPath.row]
            let imageRatio = currentImage.getImageRatio()
            print("height = \(tableView.frame.width / imageRatio)")
            return (tableView.frame.width / imageRatio)
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return curComics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("cellForRowAt")
        let cell = tableView.dequeueReusableCell(withIdentifier: "firstTableCell", for: indexPath) as! FirstTableViewCell
        cell.swipedCellIndex = indexPath
        
        if curComics.count > indexPath.row,
            let curCmc = curComics[indexPath.row] as? Comic ,
            !((curComics[indexPath.row]).isInvalidated){
            cell.setWith(curCmc)
            cell.nameLabel.sizeToFit()
            cell.nameLabel.setNeedsDisplay()
            
            
            if let p = curCmc.imageLink?.path,
                let ex = curCmc.imageLink?.curExtension,
                let all = "\(p).\(ex)" as? String {
                let arStringSplited = all.components(separatedBy: "i/mg/")
                if arStringSplited.count > 1 {
                    let searchS = arStringSplited[1]
                    print("searchS = \(searchS)")
                    if let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                        let image = self.getSavedImage(named:s) {
                        images.append(image)
                        cell.curImageView.image = image
                        cell.setNeedsDisplay()
                        cell.layoutSubviews()
                    } else {
                        let url = URL(string: all)
                        self.downloadImage(url: url!, rowNumber: indexPath)
                    }
                }
            } else {
                if let p = curCmc.thumbnail?.path,
                    let ex = curCmc.thumbnail?.curExtension,
                    let all = "\(p).\(ex)" as? String {
                    let arStringSplited = all.components(separatedBy: "i/mg/")
                    if arStringSplited.count > 1 {
                        let searchS = arStringSplited[1]
                        print("searchS = \(searchS)")
                        if let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                            let image = self.getSavedImage(named:s) {
                            images.append(image)
                            cell.curImageView.image = image
                            cell.setNeedsDisplay()
                            cell.layoutSubviews()
                        } else {
                            let url = URL(string: all)
                            self.downloadImage(url: url!, rowNumber: indexPath)
                        }
                    }
                }
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath)
    {
        
        let detailController = DetailsViewController()
        chosenComic = curComics[indexPath.row]
        detailController.curCmc = chosenComic
        print("didSelectRowAt \(String(describing: chosenComic))")
        myTableView.isUserInteractionEnabled = false
        performSegue(withIdentifier: "fromListToDetails", sender: self)

        
    }
}
