//
//  ViewController+Network.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit
import Siesta
import SwiftyJSON
import RealmSwift

extension ViewController: ResourceObserver {
    
    func saveImage(image: UIImage, url: URL, rowNumber: IndexPath) -> Bool {
        
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
            var saved = false
            DispatchQueue(label: "com.forMarvel.saveImage", qos: DispatchQoS.background).async {[weak self] () -> Void in
                do {
                    guard let strongSelf = self else { return }
                    try data.write(to: directory.appendingPathComponent("\(url.absoluteString)")!)
                    saved = true
                } catch {
                    print(error.localizedDescription)
                    saved = false
                }
            }
        return saved
        
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        let curTask = URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }
        curTask.resume()
    }
    
    func downloadImage(url: URL, rowNumber: IndexPath) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            print("error = \(String(describing: error)), data = \(String(describing: data)), response = \(String(describing: response))")
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                let arStringSplited = (url.absoluteString).components(separatedBy: "i/mg/")
                if arStringSplited.count > 1 {
                    let searchS = arStringSplited[1]
                    if data != nil,
                        searchS.count > 0,
                        let s = searchS.replacingOccurrences(of: "/", with: "_") as? String,
                        let url1 = URL(string: "\(s)"),
                        let curIm = UIImage(data: data){
                        if let res = self.saveImage(image: curIm, url: url1, rowNumber: rowNumber) as? Bool{
                            if res == true {
                                self.savedNumber = self.savedNumber + 1
                                print("savedNumber = \(self.savedNumber)")
                                self.images.append(curIm)
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.myTableView.beginUpdates()
                                    self.myTableView.reloadRows(
                                        at: [rowNumber],
                                        with: .fade)
                                    self.myTableView.endUpdates()
                                    self.myTableView.reloadData()
                                })
                            } else {
                            }
                            
                        }
                    }
                }
            }
        }
    }
        
    func getComicsResponseAction(responseGetComics: JSON)
    {
        ApiManager.getComics.removeObservers(ownedBy: self)
        if let statusGetComics = (responseGetComics["status"].string) {
            if (statusGetComics == "Ok") {
                let responseGetComicsCur: ComicsResponse   = ComicsResponse(json: responseGetComics)

                if let curRevs = responseGetComicsCur.results as List<Comic>?,
                    curRevs.count > 0
                {
                    let ar = Array(curRevs)
                    let ind = 0
                    //var curId: String = ""
                    var s = Array<String>()
                    for curEl in currFavorites {
                        s.append(curEl.id)
                    }
                    let curSetFav = Set(s)
                    for curEl in ar {
                        if let curId = curEl.id as? String,
                            curSetFav.contains(curId) {
                            ar[ind].favorite = true
                        }
                    }
                    curComics = ar
                    Database.saveEntities(entities: ar)
                }
                DispatchQueue.main.async() {
                    self.myTableView.reloadData()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.view.setNeedsDisplay()
                }
                
            } else {
                DispatchQueue.main.async() {
                    self.myTableView.reloadData()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.view.setNeedsDisplay()
                }
                let alertController = UIAlertController(title: "Error", message: (responseGetComics.description as String), preferredStyle: .alert)
                print(" responseGetBussinessCardById.rawString() = \(responseGetComics.description)")
                let cancelButtonTitle = NSLocalizedString("OK", comment: "")
                // Create the action.
                let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { action in
                    NSLog("The simple alert's cancel action occured.")
                }
                
                // Add the action.
                alertController.addAction(cancelAction)
                present(alertController, animated: true, completion: nil)
                
            }
        } else {
            
        }
    }
    
    func resourceChanged(_ resource: Resource, event: ResourceEvent) {
        print("event = \(event), resource = \(resource)")
        switch event
        {
        case .observerAdded:
            print("ViewController observerAdded")
            
        case .newData:
            DispatchQueue.main.async() {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            print("resource.typedContent() = ",resource.typedContent()!)
            if let responseGetComics: JSON = resource.typedContent(),
                resource.url.absoluteString.contains("/v1/public/comics")
            {
                self.getComicsResponseAction(responseGetComics: responseGetComics)
                
            }
            
        case .error:
            print("event = \(event), resource = \(resource)")
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print(resource.latestError?.localizedDescription as Any)
            
            let cancelButtonTitle = NSLocalizedString("OK", comment: "")
            
            let alertController = UIAlertController(title: "Error", message: resource.latestError?.userMessage, preferredStyle: .alert)
            
            // Create the action.
            let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { action in
                NSLog("The simple alert's cancel action occured.")
            }
            
            // Add the action.
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        default:
            print("Unknown network event")
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
        }
    }
    
}
