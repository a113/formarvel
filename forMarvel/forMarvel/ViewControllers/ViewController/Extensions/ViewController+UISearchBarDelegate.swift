//
//  ViewController+UISearchBarDelegate.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit

extension ViewController : UISearchBarDelegate {
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        NSLog("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func  searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NSLog("The default search bar keyboard search button was tapped: \(String(describing: searchBar.text)).")
        curComics.removeAll()
        images.removeAll()
        savedNumber = 0
        let cur:String = (searchBar.text) ?? ""
        loadResponse(searchText: cur)        
        
        let aString = searchBar.text
        _ = aString?.replacingOccurrences(of: " ", with: "+")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        NSLog("The default search bar cancel button was tapped.")
        curComics.removeAll()
        images.removeAll()
        savedNumber = 0
        searchBar.resignFirstResponder()
    }
}
