//
//  ViewController.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import UIKit
import Siesta
import SwiftyJSON
import CryptoSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var myTableView: UITableView!
    var curComics = Array<Comic>()
    var currFavorites = Array<Favorites>()
    var images = [UIImage()]
    let limit = "10"
    var savedNumber: Int = 0
    var chosenComic: Comic! = Comic()
    
    private var params = [String:String]()
    var getComicsObserver: Resource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //myTableView.register(FirstTableViewCell.self, forCellReuseIdentifier: "firstTableCell")
        myTableView.register(UINib(nibName: "FirstTableViewCell", bundle: nil), forCellReuseIdentifier: "firstTableCell")
        //myTableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "listTableCell")
        curComics.removeAll()
        images.removeAll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myTableView.isUserInteractionEnabled = true
    }
    
    func loadResponse(searchText: String)  {
        let time: String = String(Int(NSDate().timeIntervalSince1970), radix: 16, uppercase: false)
        //md5(ts+privateKey+publicKey)
        let publicKey = "790968ec6983ad5051f3cb2edb50e50c"
        let privateKey = "06ac3f13d648612c7bde12ee4fdcca7d0ede7ebf"
        let hash: String = "\(time)\(privateKey)\(publicKey)".md5()
        params =
            [
                //"titleStartsWith": self.searchBar.text,
                //"apikey": "790968ec6983ad5051f3cb2edb50e50c",
                //"ts": time,
                //"hash" : hash,
                "limit" : limit
                
        ]
        //https://gateway.marvel.com:443/v1/public/comics?titleStartsWith=love&apikey=790968ec6983ad5051f3cb2edb50e50c
        ApiManager.search = searchText
        ApiManager.ts = time
        ApiManager.hashCur = hash
        ApiManager.limitComics = "10"
        currFavorites = Array(Database.getFavoritsAll())
        getComicsObserver = ApiManager.getComics.addObserver(self)
        if let request = getComicsObserver?.request(.get  ) {
            getComicsObserver?.load(using: request)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if savedNumber == Int(limit) {
            
            DispatchQueue.main.async(execute: { () -> Void in
               self.myTableView.reloadData()
            })
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "fromListToDetails" {
            if let vc = segue.destination as? DetailsViewController {
                //let vc = segue.destination as? DetailsViewController
                //vc.curCmc = chosenComic
                let curId: String = chosenComic.id
                vc.selectedComicId = curId
            }
        }
    }


}

