//
//  Character.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class Character: Object {
    @objc dynamic var comicId = ""
    @objc dynamic var resourceURI = ""
    @objc dynamic var name = ""
    @objc dynamic var id = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    convenience init(json: JSON, idComic: String) {
        self.init()
        resourceURI            = json["resourceURI"].stringValue
        comicId = idComic
        name = json["name"].stringValue
        let arStringSplited = resourceURI.components(separatedBy: "characters/")
        if arStringSplited.count > 1 {
            let searchS = arStringSplited[1]
            print("searchS = \(searchS)")
            id = searchS
        }
    }

}
