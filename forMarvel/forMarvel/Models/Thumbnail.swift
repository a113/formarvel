//
//  Thumbnail.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class Thumbnail: Object {
    @objc dynamic var comicId = ""
    @objc dynamic var path = ""
    @objc dynamic var curExtension = ""
    
    override static func primaryKey() -> String?
    {
        return "comicId"
    }
    
    convenience init(json: JSON, idComic: String) {
        self.init()
        comicId            = idComic
        path = json["path"].stringValue
        curExtension = json["extension"].stringValue
    }

}
