//
//  CreatorId.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class CreatorId: Object {
    @objc dynamic var id: String = ""
    
    convenience init(idCur: String) {
        self.init()
        id            = idCur
        
    }
}
