//
//  ComicsResponse.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/13/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class ComicsResponse: Object {
    @objc dynamic var status = ""
    @objc dynamic var rating = 0
    var results = List<Comic>()
    
    convenience init(json: JSON) {
        self.init()
        status = json["status"].stringValue
        if let comics = (json["data"].dictionaryValue)["results"]?.arrayValue as [JSON]?,
            !comics.isEmpty
        {
            for currComic in comics
            {
                let currComicObj = Comic(json: currComic, idComic: currComic["id"].stringValue)
                self.results.append(currComicObj)
            }
        }
    }

}
