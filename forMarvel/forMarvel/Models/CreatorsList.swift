//
//  CreatorsList.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class CreatorsList: Object {
    @objc dynamic var comicId = ""
    var creatorsId = List<CreatorId>()
    
    override static func primaryKey() -> String?
    {
        return "comicId"
    }
    
    convenience init(idsCreators: [String], idComic: String) {
        self.init()
        comicId            = idComic
        for currCreatorId in idsCreators
        {
            let creatorId = CreatorId(idCur: currCreatorId)
            self.creatorsId.append(creatorId)
        }
        
    }

}
