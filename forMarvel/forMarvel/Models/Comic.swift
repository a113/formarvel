//
//  Comic.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class Comic: Object {
    @objc dynamic var id = ""
    @objc dynamic var curTitle = ""
    @objc dynamic var favorite = false
    @objc dynamic var imageLink: ImageLink? = ImageLink()
    @objc dynamic var thumbnail: Thumbnail? = Thumbnail()
    @objc dynamic var favoriteIconLink = ""
    @objc dynamic var curDescription = ""//variantDescription
    var creators = List<Creator>()
    var characters = List<Character>()
    @objc dynamic var creatorsList: CreatorsList? = CreatorsList()
    @objc dynamic var charactersList: CharacterList? = CharacterList()
    /*"creators": {
    "available": 1,
    "collectionURI": "http://gateway.marvel.com/v1/public/comics/9990/creators",
    "items": [
    {
    "resourceURI": "http://gateway.marvel.com/v1/public/creators/1523",
    "name": "Vince Colletta",
    "role": "inker"
    }
    ],
    "returned": 1
    },
    "thumbnail": {
    "path": "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
    "extension": "jpg"
    }
    "characters": {
    "available": 0,
    "collectionURI": "http://gateway.marvel.com/v1/public/comics/9990/characters",
    "items": [],
    "returned": 0
    },*/
    //var results = List<Comic>()
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    convenience init(json: JSON, idComic: String) {
        self.init()
        id = json["id"].stringValue
        if let t = json["title"].string {
            curTitle = t
        }
        curDescription = json["variantDescription"].stringValue
                if let imagesAr = json["images"].arrayValue as [JSON]?,
            !imagesAr.isEmpty
        {
            let currImageLinkObj = ImageLink(json: imagesAr[0], idComic: idComic)
            imageLink = currImageLinkObj
        } else {
            imageLink = nil
        }
        if json["thumbnail"].dictionary != nil,
            (json["thumbnail"]["path"].string?.count)! > 0,
            (json["thumbnail"]["extension"].string?.count)! > 0 {
            thumbnail = Thumbnail(json: json["thumbnail"], idComic: idComic)
        } else {
            thumbnail = nil
        }
        if let crts = (json["creators"].dictionaryValue)["items"]?.arrayValue as [JSON]?,
        !crts.isEmpty
        {
            var currCreatorsIds = Array<String>()
            for currCreator in crts
            {
                let currComicObj = Creator(json: currCreator, idComic: idComic)
                let link = currComicObj.resourceURI
                let arStringSplited = (link).components(separatedBy: "creators/")
                if arStringSplited.count > 1 ,
                    let arStringSplited1 = arStringSplited.last {
                    currCreatorsIds.append(arStringSplited1)
                }
                self.creators.append(currComicObj)
            }
            let currCreatorsId = CreatorsList(idsCreators: currCreatorsIds, idComic: idComic)
            creatorsList = currCreatorsId
        } else {
            creatorsList = nil
        }
        if let chrts = (json["characters"].dictionaryValue)["items"]?.arrayValue as [JSON]?,
            !chrts.isEmpty
        {
            var currCharactersIds = Array<String>()
            for currCharacter in chrts
            {
                let currCharacterObj = Character(json: currCharacter, idComic: idComic)
                let link = currCharacterObj.resourceURI
                let arStringSplited = (link).components(separatedBy: "characters/")
                if arStringSplited.count > 1 ,
                    let arStringSplited1 = arStringSplited.last {
                    currCharactersIds.append(arStringSplited1)
                }
                self.characters.append(currCharacterObj)
            }
            let currCharactersId = CharacterList(idsCharacters: currCharactersIds, idComic: idComic)
            charactersList = currCharactersId
        } else {
            charactersList = nil
        }
    }

}
