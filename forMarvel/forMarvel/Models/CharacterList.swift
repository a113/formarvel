//
//  CharacterList.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/14/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class CharacterList: Object {
    @objc dynamic var comicId = ""
    var charactersId = List<CharacterId>()
    
    override static func primaryKey() -> String?
    {
        return "comicId"
    }
    
    convenience init(idsCharacters: [String], idComic: String) {
        self.init()
        comicId            = idComic
        for currCharacterId in idsCharacters
        {
            let characterId = CharacterId(idCur: currCharacterId)
            self.charactersId.append(characterId)
        }
        
    }

}
