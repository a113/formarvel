//
//  Favorites.swift
//  forMarvel
//
//  Created by Andriy Kruglyanko on 11/15/18.
//  Copyright © 2018 andriyKruglyanko. All rights reserved.
//

import Foundation
import Siesta
import RealmSwift
import SwiftyJSON

class Favorites: Object {
    @objc dynamic var id: String = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
    convenience init(idCur: String) {
        self.init()
        id            = idCur
        
    }

}
